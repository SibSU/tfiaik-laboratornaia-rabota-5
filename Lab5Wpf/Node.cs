﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Wpf
{
    class Node
    {
        public string Name { get; set; }
        public Node Parent { get; set; }
        public string Info { get; set; }
        public ObservableCollection<Node> Childs { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="parent">Родитель</param>
        public Node(string name, Node parent = null)
        {
            this.Name = name;
            this.Parent = parent;
            this.Info = "";
            this.Childs = new ObservableCollection<Node>();
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="parent">Родитель</param>
        public Node(char name, Node parent = null) : this(name + "", parent) { }

        public void AddChild(Node child)
        {
            this.Childs.Add(child);
        }

        public void Clear()
        {
            this.Childs.Clear();
        }
    }
}
