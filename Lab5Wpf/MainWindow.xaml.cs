﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab5Wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dictionary<string, string> dictionary;
        string[] command = new string[] {
            "FP",
            "+FP",
            "-FP",
            "",
            "TQ",
            "*TQ",
            "/TQ",
            "",
            "#",
            "(E)",
        };

        String histories, output;

        public MainWindow()
        {
            InitializeComponent();

            dictionary = new Dictionary<string, string>();
            dictionary.Add("E", command[0]);
            dictionary.Add("P+", command[1]);
            dictionary.Add("P-", command[2]);
            dictionary.Add("P$", command[3]);
            dictionary.Add("F", command[4]);
            dictionary.Add("Q*", command[5]);
            dictionary.Add("Q/", command[6]);
            dictionary.Add("Q$", command[7]);
            dictionary.Add("T", command[8]);
            dictionary.Add("T(", command[9]);

            txtExp.Text = "(a+b)/w+d-r*d-(c+x)";
            txtCom.Text = "E ->\tFP\r\nP ->\t+FP\r\nP ->\t-FP\r\nP ->\t$\r\nF ->\tTQ\r\nQ ->\t*TQ\r\nQ ->\t/TQ\r\nQ ->\t$\r\nT ->\ta..z\r\nT ->\t(E)";

            btnSolve.Click += BtnSolve_Click;
        }

        private void BtnSolve_Click(object sender, RoutedEventArgs e)
        {
            txtLog.Text = "Цепочка	\t\t[Стек | Операция]\n";
            string Input = txtExp.Text;
            histories = "E";
            output = "E";

            Solve(Input);
        }

        private string ReplaceFirst(string s, char find, string replace)
        {
            int index = s.IndexOf(find);
            return s.Substring(0, index) + replace + s.Substring(index + 1);
        }

        private void Log(string out1, string history, string operation)
        {
            string tab;
            txtLog.Text += out1;
            if (out1.Count() >= 8)
                tab = "\t";
            if (out1.Count() >= 16)
                tab = "\t\t";
            else
                tab = "\t\t\t";
            txtLog.Text += tab + "[" + histories + " | " + operation + "]\n";
        }

        private void Solve(string input)
        {
            string temp;
            if (input.Count() == 0 && histories.Count() == 0)
                txtLog.Text += output + "\nУспех";
            else if (input.Count() != 0 && histories.Count() == 0)
                txtLog.Text += "Error\n" + 
                               "Output: " + output + "\n" +
                               "History: " + histories + "\n" +
                               "Input: " + input + "\n";
            else if (input.Count() == 0 && histories.Count() != 0)
            {
                if (dictionary.TryGetValue(histories[0] + "$", out temp))
                {
                    Log(output, histories, histories[0] + "->" + "$");
                    output = ReplaceFirst(output, histories[0], temp);
                    histories = temp + histories.Substring(1);
                    Solve(input);
                }
                else
                {
                    txtLog.Text += "Error\n" +
                               "Output: " + output + "\n" +
                               "History: " + histories + "\n" +
                               "Input: " + input + "\n";
                }
            }
            else if (histories[0] == 'E' && dictionary.TryGetValue(histories[0].ToString(), out temp))
            {
                Log(output, histories, histories[0] + "->" + temp);
                output = ReplaceFirst(output, histories[0], temp);
                histories = temp + histories.Substring(1);
                Solve(input);
            }
            else if (input[0] >= 97 && input[0] <= 122)
            {
                if (dictionary.TryGetValue(histories[0].ToString(), out temp))
                {
                    if (temp[0] == '#')
                    {
                        Log(output, histories, histories[0] + "->" + input[0]);
                        output = ReplaceFirst(output, histories[0], input[0].ToString());
                        histories = histories.Substring(1);
                        Solve(input.Substring(1));
                    }
                    else
                    {
                        Log(output, histories, histories[0].ToString() + "->" + temp);
                        output = ReplaceFirst(output, histories[0], temp);
                        histories = temp + histories.Substring(1);
                        Solve(input);
                    }
                }
            }
            else
            {
                if (input[0] == histories[0])
                {
                    // Log(output, histories, histories + "->" + histories.Substring(1));
                    histories = histories.Substring(1);
                    Solve(input.Substring(1));
                }
                else if (input[0] == 40)
                {
                    if (dictionary.TryGetValue(histories[0].ToString() + input[0], out temp) || dictionary.TryGetValue(histories[0].ToString(), out temp))
                    {
                        Log(output, histories, histories[0].ToString() + input[0] + "->" + temp);
                        output = ReplaceFirst(output, histories[0], temp);
                        histories = temp + histories.Substring(1);
                        Solve(input);
                    }
                    else
                    {
                        txtLog.Text += "Error\n" +
                                   "Output: " + output + "\n" +
                                   "History: " + histories + "\n" +
                                   "Input: " + input + "\n";
                    }
                }
                else if (dictionary.TryGetValue(histories[0].ToString() + input[0], out temp))
                {
                    Log(output, histories, histories[0].ToString() + input[0] + "->" + temp);
                    output = ReplaceFirst(output, histories[0], temp);
                    histories = temp + histories.Substring(1);
                    Solve(input);
                }
                else if (dictionary.TryGetValue(histories[0] + "$", out temp))
                {
                    Log(output, histories, histories + " | " + histories[0] + "->" + "$");
                    output = ReplaceFirst(output, histories[0], temp);
                    histories = temp + histories.Substring(1);
                    Solve(input);
                }
                else if (input[0] == 41)
                {
                    if (dictionary.TryGetValue(histories[0].ToString() + input[0], out temp))
                    {
                        // Log(output, histories, histories + " | " + histories[0] + "->" + histories.Substring(1));
                        histories = histories.Substring(1);
                        Solve(input.Substring(1));
                    }
                    else
                    {
                        txtLog.Text += "Error\n" +
                                   "Output: " + output + "\n" +
                                   "History: " + histories + "\n" +
                                   "Input: " + input + "\n";
                    }
                }
            }
        }
    }
}